<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-widgets">
			<div class="container group">
				<div class="footer-nav-wrap group">	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-navigation-1', 'menu_class' => 'footer-menu', ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-navigation-2', 'menu_class' => 'footer-menu', ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-navigation-3', 'menu_class' => 'footer-menu', ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-navigation-4', 'menu_class' => 'footer-menu', ) ); ?>
				</div>
				<div class="footer-address">
					<p>Address<br/>
					<?php the_field('address','option'); ?>
					</p>
					<p>Phone<br/><a href="tel:<?php the_field('telephone','option'); ?>"><?php the_field('telephone','option'); ?></a></p>
				</div>
				<div class="footer-subscribe">
					<?php dynamic_sidebar( 'mailchimp-widget' ); ?>
				</div>
			</div>
		</div>
		<div class="site-info">
			<div class="container group">
				<span><?php the_field( 'copyright','option' ); ?></span>
				<?php wp_nav_menu( array( 'theme_location' => 'bottom-navigation', 'menu_class' => 'bottom-menu', ) ); ?>
				<span class="christom">Design by <a href="http://www.christom.com.au" target="_blank" rel="nofollow">Christom</a></span>
			</div>
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
