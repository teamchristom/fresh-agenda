<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php fa_banner(); ?>

			<div class="container group">
				
				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<?php if(function_exists('bcn_display'))
					{
					bcn_display();
					}?>
				</div>

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h1>
					</header><!-- .page-header -->

					<?php
					// Start the loop.
					while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('group'); ?>>
									<?php the_post_thumbnail('post-thumb');?>
									<header class="entry-header">
										<?php
											if ( is_single() ) :
												the_title( '<h1 class="entry-title">', '</h1>' );
											else :
												the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
											endif;
										?>
									</header><!-- .entry-header -->
									<div class="entry-date">
										<span><?php the_time('m/d/Y'); ?></p></span>
									</div>
									<div class="entry-content">
										<?php if ( has_excerpt() ) {
										      echo '<p>'.excerpt(29).'</p>';
										} else { 
										      echo '<p>'.content(29).'</p>';
										}
										?>
									<a href="<?php the_permalink(); ?>">Read article</a>
									</div><!-- .entry-content -->

								</article><!-- #post-## -->
								<?php
						// End the loop.
						endwhile;

						// Previous/next page navigation.
							the_posts_pagination( array(
								'prev_text'          => __( 'Previous', 'twentyfifteen' ),
								'next_text'          => __( 'Next', 'twentyfifteen' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
							) );

					// If no content, include the "No posts found" template.
					else :
					get_template_part( 'content', 'none' );

				endif;
				?>
			</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
