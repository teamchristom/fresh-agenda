<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/***LOAD CORE FILES DO NOT DELETE START***/
require_once( 'functions/enqueue.php' );
require_once( 'functions/default.php' );
require_once( 'functions/theme.php' );
require_once( 'functions/scripts.php' );
require_once( 'functions/acf.php' );
/***LOAD CORE FILES DO NOT DELETE END***/
