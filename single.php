<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php fa_banner(); ?>

			<div class="container group">

				<div class="left-col">

					<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
						<?php if(function_exists('bcn_display'))
						{
						bcn_display();
						}?>
					</div>

					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					// End the loop.
					endwhile;
					?>
				</div>
				<aside id="secondary" class="right-col">
					<?php dynamic_sidebar( 'blog-widget' ); ?>
				</aside>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
