<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**********************************************/
/* ENQUEUE STYLES & SCRIPTS                   */
/**********************************************/
function tnv_scripts() {
	wp_enqueue_style ( 'normalize', get_stylesheet_directory_uri() . '/assets/css/normalize.css', array() );
	wp_enqueue_style ( 'fontawesome', get_stylesheet_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', array() );
	wp_enqueue_script( 'hoverintent', get_stylesheet_directory_uri() . '/assets/superfish/hoverIntent.js',array('jquery') );
	wp_enqueue_style( 'sf', get_stylesheet_directory_uri() . '/assets/superfish/superfish.css',array() );
	wp_enqueue_script( 'sf', get_stylesheet_directory_uri() . '/assets/superfish/superfish.js',array('hoverintent') );
	wp_enqueue_style( 'flex', get_stylesheet_directory_uri() . '/assets/flexslider/flexslider.css',array() );
	wp_enqueue_script( 'flex', get_stylesheet_directory_uri() . '/assets/flexslider/jquery.flexslider-min.js',array('jquery') );
	wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/assets/slicknav/slicknav.css',array() );
	wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/assets/slicknav/jquery.slicknav.min.js',array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'tnv_scripts' );
