<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**********************************************/
/* THEME FUNCTIONS                          */
/**********************************************/
/***IMAGE SIZES START***/
if ( function_exists('add_theme_support') ) { 
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'home-box', 352, 126, true );
	add_image_size( 'top-banner', 9999, 263, true );
	add_image_size( 'post-thumb', 160, 140, true );
	add_image_size( 'home-post-thumb', 90, 79, true );
	add_image_size( 'block-text-header', 1100, 9999, true );
	add_image_size( 'block-image-link', 263, 110, true );
	add_image_size( 'anchor-link-image', 351, 150, true );
}
/***IMAGE SIZES END***/

function ct_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Mailchimp' ),
		'id' => 'mailchimp-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name' => __( 'Blog' ),
		'id' => 'blog-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name' => __( 'Twitter Feed' ),
		'id' => 'twitter-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );
}


add_action( 'widgets_init', 'ct_widgets_init' );

/***MENU LOCATIONS START***/
function fa_menus() {
  register_nav_menus(
    array(  
        'main-navigation'   => __( 'Main Navigation' ),
        'footer-navigation-1'   => __( 'Footer Navigation #1' ),
        'footer-navigation-2'   => __( 'Footer Navigation #2' ),
        'footer-navigation-3'   => __( 'Footer Navigation #3' ),
        'footer-navigation-4'   => __( 'Footer Navigation #4' ),
        'bottom-navigation'   => __( 'Bottom Navigation' )
    )
  );
} 
add_action( 'init', 'fa_menus' );
/***MENU LOCATIONS END***/

function fa_banner() {
	?>
	<?php if( get_field('banner') ) : ?>
		<?php $attachment_id = get_field('banner');
			$size = "top-banner";
			$image = wp_get_attachment_image_src( $attachment_id, $size );
		?>
		<div class="top-banner" style="background-image:url('<?php echo $image[0]; ?>');"></div>
	<?php else : ?>
		<?php $attachment_id = get_field('default_banner','option');
			$size = "top-banner";
			$image = wp_get_attachment_image_src( $attachment_id, $size );
		?>
		<div class="top-banner" style="background-image:url('<?php echo $image[0]; ?>');"></div>
	<?php endif; ?>
	<?php
}

function acf_anchor() {
	?>
	<?php if( get_sub_field('anchored_section') ) : ?>
		id="<?php the_sub_field('section_name'); ?>"
	<?php endif; ?>
	<?php
}

function acf_backtotop() {
	?>
	<?php if( get_sub_field('anchored_section') ) : ?>
		<a class="back-to-top" href="#top">Back to top</a>
	<?php endif; ?>
	<?php
}


function acf_full_bg() {
	?>
	<?php if( get_sub_field('full_width_background_section') ) {
		echo 'full-width-section';
	} ?>
	<?php 
}

function acf_full_bg_color() {
	?>
	<?php if( get_sub_field('background_color') && get_sub_field('full_width_background_section') ) : ?>
		style="background-color:<?php the_sub_field('background_color'); ?>;"
	<?php endif; ?>
	<?php
}

function acf_bordered() {
	?>
	<?php if( get_sub_field('bordered') && get_sub_field('full_width_background_section') ) {
		echo 'bordered';
	} ?>
	<?php
}

function acf_link_type() {
	?>
	<?php if( get_sub_field('boxed_text_link_type') == 'link' ) : ?> 
		<?php the_sub_field('boxed_text_link_link'); ?> 
	<?php else : ?>
 		#<?php the_sub_field('boxed_text_link_anchor'); ?>
 	<?php endif; ?>
	<?php
}

function acf_text_block() {
	?>
	<div class="acf-text-block acf-block">
	<?php if( get_sub_field('text_header_image') ) : ?>
		<?php
			$attachment_id = get_sub_field('text_header_image');
			$size = "block-text-header"; // (thumbnail, medium, large, full or custom size)
			$image = wp_get_attachment_image_src( $attachment_id, $size );
		?>
		<div class="acf-text-header-block" style="background-image:url('<?php echo $image[0]; ?>')"></div>
	<?php endif; ?>
	<?php the_sub_field('text_content'); ?>
	</div>
	<?php
}

function acf_large_blue_text_block() {
	?>
	<div class="acf-large-blue-text-block acf-block">
	<?php the_sub_field('large_blue_text_content') ?>
	</div>
	<?php
}

function acf_boxed_text_link_block() {
	?>
	<div class="acf-boxed-text-link-block acf-block group">
	<?php while ( have_rows('boxed_text_link_repeater') ) : the_row(); ?>
		<a class="box flex_row_center" href="<?php acf_link_type(); ?>">
			<?php the_sub_field('boxed_text_link_text'); ?>
			<span></span>
		</a>
	<?php endwhile; ?>
	</div>
	<?php
}

function acf_boxed_image_link_type() {
	?>
	<?php if( get_sub_field('boxed_image_link_type') == 'link' ) : ?>
		<?php the_sub_field('boxed_image_link_link'); ?>
	<?php else : ?>
		<?php the_sub_field('boxed_image_link_attachment'); ?>
	<?php endif; ?>
	<?php
}

function acf_boxed_image_link_block() {
	?>
	<div class="acf-boxed-image-link-block acf-block group">
	<?php while ( have_rows('boxed_image_link_repeater') ) : the_row(); ?>
		<a class="box" href="<?php acf_boxed_image_link_type(); ?>" <?php if( get_sub_field('boxed_image_link_type') == 'attachment' ) { echo 'target="_blank"'; } ?>>
			<?php
			$attachment_id = get_sub_field('boxed_image_link_image');
			$size = "block-image-link"; // (thumbnail, medium, large, full or custom size)
			$image = wp_get_attachment_image_src( $attachment_id, $size );
			?>
			<img src="<?php echo $image[0]; ?>" />
			<span><?php the_sub_field('boxed_image_link_title'); ?></span>
		</a>
	<?php endwhile; ?>
	</div>
	<?php
}

function acf_top_anchor_links_block() {
	?>
	<div class="acf-top-anchor-links-block acf-block group">
	<?php while ( have_rows('top_anchor_links_repeater') ) : the_row(); ?>
		<a class="box" href="#<?php the_sub_field('top_anchor_links_link'); ?>">
			<?php
			$attachment_id = get_sub_field('top_anchor_links_image');
			$size = "anchor-link-image"; // (thumbnail, medium, large, full or custom size)
			$image = wp_get_attachment_image_src( $attachment_id, $size );
			?>
			<img src="<?php echo $image[0]; ?>" />
			<span><?php the_sub_field('top_anchor_links_title'); ?></span>
		</a>
	<?php endwhile; ?>
	</div>
	<?php
}

function kp_block() {
	?>
	<div class="acf-one-col group full-width-section kp-module" style="background-color:#1e73be;">
		<div class="acf-block group">
			<a class="kp-box" href="<?php the_field('kp_link_1'); ?>">
				<?php $attachment_id = get_field('kp_image_1');
					$size = "home-box"; // (thumbnail, medium, large, full or custom size)
					$image = wp_get_attachment_image_src( $attachment_id, $size );
				?>
				<img src="<?php echo $image[0]; ?>" />
				<span></span>
			</a>
			<a class="kp-box" href="<?php the_field('kp_link_2'); ?>">
				<?php $attachment_id = get_field('kp_image_2');
					$size = "home-box"; // (thumbnail, medium, large, full or custom size)
					$image = wp_get_attachment_image_src( $attachment_id, $size );
				?>
				<img src="<?php echo $image[0]; ?>" />
				<span></span>
			</a>
			<div class="kp-box-text">
				<a class="kp-text" href="<?php the_field('kp_text_link_1'); ?>"><?php the_field('kp_text_1'); ?><span></span></a>
				<a class="kp-text" href="<?php the_field('kp_text_link_2'); ?>"><?php the_field('kp_text_2'); ?><span></span></a>
			</div>
		</div>
	</div>
	<?php
}

// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { 
		background-image: url('.get_stylesheet_directory_uri().'/images/freshagenda.jpg) !important;
		background-position: center top !important;
		background-repeat: no-repeat !important;
		background-size: 270px 64px !important;
		display: block !important;
		height: 64px !important;
		margin: 0 auto !important;
		overflow: hidden !important;
		padding-bottom: 0 !important;
		text-indent: -9999px !important;
		width: 270px !important;
	}

	body.login {
		background: none repeat scroll 0 0 #FFFFFF;
	}
	</style>';
}
add_action('login_head', 'custom_login_logo');

function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_stylesheet_directory_uri().'/images/favicon.ico" />';
}

add_action('wp_head', 'blog_favicon');
// add a favicon for your admin

function admin_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_stylesheet_directory_uri(). '/images/favicon.ico" />';
}

add_action('admin_head', 'admin_favicon');