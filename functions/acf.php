<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**********************************************/
/* ACF FUNCTIONS                          */
/**********************************************/
/***ACF START***/
add_filter( 'acf/settings/path', 'theme_acf_settings_path' );
function theme_acf_settings_path( $path ) {
    $path = get_stylesheet_directory() . '/assets/acf/';
    return $path;
}
add_filter('acf/settings/dir', 'theme_acf_settings_dir' );
function theme_acf_settings_dir( $dir ) {
    $dir = get_stylesheet_directory_uri() . '/assets/acf/';
    return $dir;
}
add_filter('acf/settings/show_admin', '__return_false');
include_once( get_stylesheet_directory() . '/assets/acf/acf.php' );

if( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
/***ACF END***/