<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**********************************************/
/* DEFAULT FUNCTIONS                          */
/**********************************************/
function ct_remove_parent_theme_locations(){
    unregister_nav_menu( 'primary' );
    unregister_nav_menu( 'social' );
}
add_action( 'admin_init', 'ct_remove_parent_theme_locations' );

function ct_remove_widgets(){
    unregister_sidebar( 'sidebar-1' );
}
add_action( 'widgets_init', 'ct_remove_widgets',11 );

/***REMOVE CUSTOMIZER START***/
function diversify_remove_customize_page(){
	global $submenu;
	unset( $submenu['themes.php'][6] ); // remove customize link
}
add_action( 'admin_menu', 'diversify_remove_customize_page' );

// Remove 'Customize' from the Toolbar -front-end
function diversify_remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('customize');
}
add_action( 'wp_before_admin_bar_render', 'diversify_remove_admin_bar_links' );

// Add Custom CSS to Back-end head
function diversify_admin_css() {
    echo '<style type="text/css">.hide-if-no-customize { display:none !important; } </style>';
}
add_action('admin_head', 'diversify_admin_css');
/***REMOVE CUSTOMIZER END***/

add_action( 'after_setup_theme', 'remove_parent_theme_support', 11 ); 

function remove_parent_theme_support() {
	remove_theme_support( 'custom-background' );
	remove_theme_support( 'custom-header' );
  remove_theme_support( 'automatic-feed-links' );
  remove_theme_support( 'post-formats' );
}
/***REMOVE THEME SUPPORT END***/

/***LIMITER***/
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

function is_blog () {
  global  $post;
  $posttype = get_post_type($post );
  return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}

/***CHRISTOM DEFAULTS***/
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'functions.php' == basename($_SERVER['SCRIPT_FILENAME']))

{

die ('No access!');

}
function wps_admin_bar() {

global $wp_admin_bar;

$wp_admin_bar->remove_menu('wp-logo');

$wp_admin_bar->remove_menu('updates');

$wp_admin_bar->remove_menu('comments');

$wp_admin_bar->remove_menu('new-content');

$wp_admin_bar->remove_menu('wporg');

//$wp_admin_bar->remove_menu('view-site');

}

add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );
function remove_dashboard_widgets() {

global $wp_meta_boxes;

unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);

unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);

unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);

unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);

unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);

unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);

unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
add_filter( 'contextual_help', 'wpse50723_remove_help', 999, 3 );

function wpse50723_remove_help($old_help, $screen_id, $screen){

$screen->remove_help_tabs();

return $old_help;

}
/**
* remove unwanted menus
*/

function remove_menus () {

global $menu;

$restricted = array(__('Dashboard'), __('Links'));

end ($menu);

while (prev($menu)){

$value = explode(' ',$menu[key($menu)][0]);

if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}

}

}

add_action('admin_menu', 'remove_menus');
// remove junk from head

remove_action('wp_head', 'rsd_link');

remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'feed_links', 2);

remove_action('wp_head', 'index_rel_link');

remove_action('wp_head', 'wlwmanifest_link');

remove_action('wp_head', 'feed_links_extra', 3);

remove_action('wp_head', 'start_post_rel_link', 10, 0);

remove_action('wp_head', 'parent_post_rel_link', 10, 0);

remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
