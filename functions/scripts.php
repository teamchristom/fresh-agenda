<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**********************************************/ 
/* SCRIPTS                                    */
/**********************************************/
function tnv_js() {
?>
<!-- SUPERFISH -->
<script type="text/javascript">
	var xsf = jQuery.noConflict();
	xsf(document).ready(function() {
		xsf("#menu-main-nav").superfish({
			cssArrows:  false,
		});
	});
</script>

<!-- FLEX -->
<script type="text/javascript" charset="utf-8">
	var xflex = jQuery.noConflict();
	xflex(window).load(function() {
		xflex('.flexslider').flexslider({
    		animation: "slide",
    		controlNav: false
  		});
	});
</script>

<script type="text/javascript">
	var xslick = jQuery.noConflict();
	xslick('.sf-menu').slicknav({
			prependTo:'.nav-mobile',
			label:'',
			allowParentLinks: true,
			//closedSymbol:"&#9660;",
			//openedSymbol:"&#9658;"	
	});
</script>

<script type="text/javascript" charset="utf-8">
var xss = jQuery.noConflict();
xss('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = xss(this.hash);
        target = target.length ? target : xss('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             xss('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});
</script>
<?php }
add_action( 'wp_footer', 'tnv_js' );
	