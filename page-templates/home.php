<?php
/**
 * Template Name: Home Template
 **/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="home-box-wrap">
				<div class="container group">
				<?php while ( have_rows('home_boxes') ) : the_row(); ?>
					<a class="home-box" href="<?php the_sub_field('home_box_link'); ?>">
						<?php $attachment_id = get_sub_field('home_box_image');
							$size = "home-box"; // (thumbnail, medium, large, full or custom size)
							$image = wp_get_attachment_image_src( $attachment_id, $size );
						?>
						<img src="<?php echo $image[0]; ?>" />
						<span></span>
					</a>
				<?php endwhile; ?>
				</div>
			</div>

			<div class="home-module-wrap">
				<div class="container group">
					<div class="col">
						<h2>Blog Articles</h2>
						<?php
							$args = array(
								'post_type'=> 'post',
								'posts_per_page'   => 3,
								'order'    => 'DESC'
							);
							query_posts( $args );

							while ( have_posts() ) : the_post(); ?>
							
							<article id="post-<?php the_ID(); ?>" <?php post_class('group'); ?>>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home-post-thumb');?></a>
								<header class="entry-header">
									<?php
										if ( is_single() ) :
											the_title( '<h1 class="entry-title">', '</h1>' );
										else :
											the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
										endif;
									?>
								</header><!-- .entry-header -->
								<div class="entry-content">
									<?php if ( has_excerpt() ) {
									      echo '<p>'.excerpt(13).'</p>';
									} else { 
									      echo '<p>'.content(13).'</p>';
									}
									?>
								</div><!-- .entry-content -->

							</article><!-- #post-## -->

						<?php endwhile; wp_reset_query(); ?>
						<p style="text-align: right;"><a href="/blog">View more articles</a></p>
					</div>
					<div class="col">
						<h2>News and Updates</h2>
						<?php the_field('nau_content',4); ?>
					</div>
					<div class="col">
						<h2>Twitter Feeds</h2>
						<?php dynamic_sidebar( 'twitter-widget' ); ?>
					</div>
				</div>
			</div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
