<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php fa_banner(); ?>

			<div id="top" class="container group">

				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<?php if(function_exists('bcn_display'))
					{
					bcn_display();
					}?>
				</div>

				<?php while ( have_posts() ) : the_post(); ?>

					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						
					<?php if( have_rows('builder') ): ?>

						<?php while ( have_rows('builder') ) : the_row(); ?>

							<div class="acf-row group">

							<?php if( get_row_layout() == 'one_column' ): ?>

								<div class="acf-one-col group<?php acf_full_bg(); acf_bordered(); ?>"<?php acf_full_bg_color(); ?><?php acf_anchor(); ?>>

								<?php if( get_sub_field('section_title') ) : ?>
									<h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
								<?php endif; ?>

								<?php if( have_rows('block') ): ?>

									<?php while ( have_rows('block') ) : the_row(); ?>

										<?php if( get_row_layout() == 'text' ): ?>

											<?php acf_text_block(); ?>

										<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

											<?php acf_large_blue_text_block(); ?>

										<?php elseif( get_row_layout() == 'boxed_text_link' ): ?>

											<?php acf_boxed_text_link_block(); ?>

										<?php elseif( get_row_layout() == 'boxed_image_link' ): ?>

											<?php acf_boxed_image_link_block(); ?>

										<?php elseif( get_row_layout() == 'top_anchor_links' ): ?>

											<?php acf_top_anchor_links_block(); ?>

										<?php endif; ?>

									<?php endwhile; //loop block ?>

								<?php endif; ?>
								<?php acf_backtotop(); ?>
							</div>

							<?php elseif( get_row_layout() == 'two_column' ): ?>

								<div class="acf-two-col group<?php acf_full_bg(); acf_bordered(); ?>"<?php acf_full_bg_color(); ?><?php acf_anchor(); ?>>

									<?php if( get_sub_field('section_title') ) : ?>
									<h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
									<?php endif; ?>
									<div class="acf-col-wrap">
									<?php if( have_rows('block_aa') ): ?>
										<div class="acf-col">
											<?php while ( have_rows('block_aa') ) : the_row(); ?>

											<?php if( get_row_layout() == 'text' ): ?>

												<?php acf_text_block(); ?>

											<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

												<?php acf_large_blue_text_block(); ?>

											<?php endif; ?>

											<?php endwhile; //loop block ?>
										</div>
									<?php endif; ?>

									<?php if( have_rows('block_ab') ): ?>
										<div class="acf-col">
											<?php while ( have_rows('block_ab') ) : the_row(); ?>

											<?php if( get_row_layout() == 'text' ): ?>

												<?php acf_text_block(); ?>

											<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

												<?php acf_large_blue_text_block(); ?>

											<?php endif; ?>
											
											<?php endwhile; //loop block ?>
										</div>
									<?php endif; ?>
									</div>
									<?php acf_backtotop(); ?>
								</div>

							<?php elseif( get_row_layout() == 'three_column' ): ?>

								<div class="acf-three-col group<?php acf_full_bg(); acf_bordered(); ?>"<?php acf_full_bg_color(); ?><?php acf_anchor(); ?>>

									<?php if( get_sub_field('section_title') ) : ?>
									<h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
									<?php endif; ?>
									<div class="acf-col-wrap">
									<?php if( have_rows('block_ba') ): ?>
										<div class="acf-col">
											<?php while ( have_rows('block_ba') ) : the_row(); ?>

											<?php if( get_row_layout() == 'text' ): ?>

												<?php acf_text_block(); ?>

											<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

												<?php acf_large_blue_text_block(); ?>

											<?php endif; ?>

											<?php endwhile; //loop block ?>
										</div>
									<?php endif; ?>

									<?php if( have_rows('block_bb') ): ?>
										<div class="acf-col">
											<?php while ( have_rows('block_bb') ) : the_row(); ?>

											<?php if( get_row_layout() == 'text' ): ?>

												<?php acf_text_block(); ?>

											<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

												<?php acf_large_blue_text_block(); ?>

											<?php endif; ?>
											
											<?php endwhile; //loop block ?>
										</div>
									<?php endif; ?>

									<?php if( have_rows('block_bc') ): ?>
										<div class="acf-col">
											<?php while ( have_rows('block_bc') ) : the_row(); ?>

											<?php if( get_row_layout() == 'text' ): ?>

												<?php acf_text_block(); ?>

											<?php elseif( get_row_layout() == 'large_blue_text' ): ?>

												<?php acf_large_blue_text_block(); ?>

											<?php endif; ?>
											
											<?php endwhile; //loop block ?>
										</div>
									<?php endif; ?>
									</div>
									<?php acf_backtotop(); ?>
								</div>

							<?php endif; ?>

							</div>

						<?php endwhile; //loop row ?>

					<?php endif; ?>

					<?php if( is_page('knowledge-products') ) : ?>
						<?php kp_block(); ?>
					<?php endif;?>

					</div><!-- .entry-content -->
				<?php endwhile; //loop wp ?>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
