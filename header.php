<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
	<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-47446511-1"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

	<div class="topbar flex_row_center">
		<div class="container group">
			<div class="sm-bar group">
				<a class="sm lnkdn" href="<?php the_field('linkedin','option'); ?>" target="_blank"></a>
				<a class="sm tw" href="<?php the_field('twitter','option'); ?>" target="_blank"></a>
			</div>
			<div class="search-bar">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
					<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( '', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					<button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
				</form>
			</div>
			<div class="telephone"><a href="tel:<?php the_field('telephone','option'); ?>"><?php the_field('telephone','option'); ?></a></div>
		</div>
	</div>

	<header id="masthead" class="site-header" role="banner">
		<div class="container group flex_row_center">
			<div class="site-branding">
				<?php
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;
				?>
			</div><!-- .site-branding -->
			<div class="nav-desktop">
				<?php wp_nav_menu( array( 'theme_location' => 'main-navigation', 'menu_class' => 'sf-menu', ) ); ?>
			</div>
			<div class="nav-mobile"></div>
		</div>
	</header><!-- .site-header -->

	<?php if( is_front_page() ) {	
		the_field('slider_shortcode',4);
	} ?>

	<div id="content" class="site-content">
