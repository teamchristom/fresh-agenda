<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php fa_banner(); ?>

			<div class="container group">
				
				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<?php if(function_exists('bcn_display'))
					{
					bcn_display();
					}?>
				</div>

				

					<?php if ( have_posts() ) : ?>

						<header class="page-header">
							<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="taxonomy-description">', '</div>' );
							?>
						</header><!-- .page-header -->
							<div class="left-col">
						<?php
						// Start the Loop.
						while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('group'); ?>>
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumb');?></a>
									<header class="entry-header">
										<?php
											if ( is_single() ) :
												the_title( '<h1 class="entry-title">', '</h1>' );
											else :
												the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
											endif;
										?>
									</header><!-- .entry-header -->
									<div class="entry-date">
										<span><?php the_time('m/d/Y'); ?></p></span>
									</div>
									<div class="entry-content">
										<?php if ( has_excerpt() ) {
										      echo '<p>'.excerpt(29).'</p>';
										} else { 
										      echo '<p>'.content(29).'</p>';
										}
										?>
									<a href="<?php the_permalink(); ?>">Read article</a>
									</div><!-- .entry-content -->

								</article><!-- #post-## -->
								<?php
						// End the loop.
						endwhile;

						// Previous/next page navigation.
							the_posts_pagination( array(
								'prev_text'          => __( 'Previous', 'twentyfifteen' ),
								'next_text'          => __( 'Next', 'twentyfifteen' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
							) );

					// If no content, include the "No posts found" template.
					else :
						get_template_part( 'content', 'none' );

					endif;
					?>
				</div>
				<aside id="secondary" class="right-col">
					<?php dynamic_sidebar( 'blog-widget' ); ?>
				</aside>
			</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
